package main
import "fmt"

import "sort"

func isprime(n int) bool {
	for i := 2; i <= n/2; i++ {
		if n%i==0 {
			return false
		}
	}
	return true
}

func sqrt(n int) int {
	for i := 1; i<= n/2; i++ {
		if i * i <= n && (i+1) * (i+1) > n {
			return i
		}
	}
	return -1
}

func divisors(n int) []int {
	res := make([]int,0)
	for i := 2; i <= sqrt(n); i++ {
		if n % i == 0 && isprime(i) {
			res = append(res, i)
		
		
			if i != n / i && isprime(n/i) {
				res = append(res, n/i)
			} 
		}
	}
	sort.Ints(res)
	return res
}

func min(a int, b int) int {
	if a <=b {
		return a
	}
	return b
}

func Solution(A []int, B []int) int {
    // write your code in Go 1.4
	resA := make([][]int,0)
	for i := range A {
	    resA = append(resA, divisors(A[i]))
	}

    resB := make([][]int,0)
    for j := range B {
        resB = append(resB, divisors(B[j]))
    }

	c := 0
	for k := range resA {
		var l1, l2 = len(resA[k]), len(resB[k])
		m := min(l1,l2)

		c2 := 0
		for l := 0; l<m; l++ {
		
			if resA[k][l] == resB[k][l] {
				c2++
			}
		}
		if m == l1 && m == l2 && c2 == m {
			c++
		}
    }
    return c
}

func main() {
	fmt.Println(Solution([]int{15,10,9}, []int{75,30,5}))
}
